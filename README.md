# CITES Coding Contest 2016

## Problema

Esta carpeta contiene la solución al problema "La Inclinación de un Cilindro". A partir de la consigna era necesario responder:
  1. ¿Cuál es la altura del cilindro?
  2. ¿Cuál es el radio del cilindro?
  3. ¿Cuál es la inclinación del eje del cilindro en radianes con respecto al eje z ?


## Repuestas

Debido a la naturaleza de los datos dados, los valores pedidos se pudieron obtener solo de forma aproximada:

 * La altura aproximada del cilindro es 17.213869
 * El radio aproximado del cilindro es 7.359682
 * La inclinación aproximada respecto del eje Z es 0.571016 rad (32.716805 grados)


## Metodología

Para resolver el problema se automatizó el análisis del conjunto de puntos dato mediante el software de procesamiento Octave. El script resultante se encuentra en el archivo cilindro.m.

El script realiza la carga de los datos del cilindro y determina las coordenadas aproximadas del centro de masa del mismo. Conociendo estas coordenadas, el valor del radio del cilindro es aproximadamente igual a la distancia entre el centro del mismo y el más cercano de los puntos dados. De forma semejante, ya conociendo el radio se puede determinar la altura aproximada medianta la aplicación de Pitágoras a la distancia del punto más alejado al centro. 

La determinación de la inclinación respecto del eje del cilindro respecto del eje Z es un poco más compleja. Para lograr esto primeramente se intersectó el cilindro con una esfera de radio mayor que el radio del cilindro pero menor que su altura, y se eliminaron todos los puntos del cilindro que caían dentro de la misma. Esto dejó dos nubes de puntos separadas, correspondientes a los extremos del cilindro, cuyos miembros se pueden discriminar a partir del signo de la coordenada Z de cada uno de ellos (esto es así por la forma en que se eligió el radio de la esfera y por la posición inicial del cilindro).

Se armo un subconjunto de puntos por cada una de estas nubes, formando así un grupo de puntos por cada extremo del cilindro. Por la simetría del cilindro, los centros de masa de ambos subconjuntos son puntos que se encuentran sobre la recta que es eje de simetría del cilindro. Eso permitió encontrar un vector colineal con el eje del cilindro a partir de conocer las coordenadas de estos dos centros de masa.

Ya con este vector a mano, el cálculo de la inclinación respecto del eje Z se llevó a cabo normalizando el vector y calculando el producto escalar entre el mismo y el vector unitario (0, 0, 1), el cual es colineal con el eje Z. El producto de ambos por definición es el coseno del ángulo que forman, por lo que la obtención del valor del ángulo de inclinación a partir de ahí es trivial.

## Cómo ejecutar las Soluciones

Para ejecutar las soluciones se debe ejecutar el script cilindro.m desde Octave o Matlab.

El script carga los datos del archivo cilindro.txt, por lo que dicho archivo debe estar en el directorio actual al ejecutarlo.

## Repositorio Git de la solución

Todos los archivos de la solución pueden hallarse en

https://gitlab.com/glpuga/acodearlainclinaciondeuncilindro.git



