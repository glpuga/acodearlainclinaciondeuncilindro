clc
clear 
close all

fprintf('\n');

% %%%%%%%%%%%%%%%%%%%%%%%
%
% Carga de los datos dados del cilindro 
v = load('cilindro.txt');

x = v(:,1);
y = v(:,2);
z = v(:,3);

% %%%%%%%%%%%%%%%%%%%%%%%
%
% Primera observación de los datos para tantear la situación.
%
% %%%

figure;

plot3(x, y, z, '*');
axis equal
grid on
xlabel('X')
ylabel('Y')
zlabel('Z')
title('Representación de los datos dados');

% Hay rotación en al menos dos ejes diferentes

% %%%%%%%%%%%%%%%%%%%%%%%
%
% Para simplificar las operaciones siguientes, relativizo las coordenadas
% del cilindro respecto de su centro.
%
% %%%

% Dada la cantidad de puntos, suponiendo uniforme su distribucion, 
% asumo que puedo determinar el centro del cilindro promediando sus
% coordenadas
xc = mean(x);
yc = mean(y);
zc = mean(z);

% Determino las coordenadas relativas de cada uno de los puntos relativas 
% al centro del cilindro
xr = x - xc;
yr = y - yc;
zr = z - zc;

% Grafico el resultado
figure;
plot3(xr, yr, zr, '.');
axis equal
grid on
xlabel('Xrel')
ylabel('Yrel')
zlabel('Zrel')
title('Cambio de coordenadas relativo al centro del cilindro');

% %%%%%%%%%%%%%%%%%%%%%%%
%
% Inicio el proceso de determinar la altura y el radio del cilindro
% dado
%
% %%%

% determino las distancias de cada punto al centro del cilindro
m = sqrt(xr.^2 + yr.^2 + zr.^2);
    
% Determino la distancia del punto más cercano y más lejano, respecto
% del centro de coordenadas    
maxMod = max(m);
minMod = min(m);

% El radio es directamente la distancia al punto más cercano a partir del
% centro.
radio = minMod;

% La altura la calculo por Pitágoras. La distancia más larga del centro a
% cualquier punto del cilindro es una aproximación de la distancia del
% centro a cualquier punto de la periferia de uno de los extremos del
% cilindro, y por lo tanto de la hipotenusa de un triángulo que tiene un
% cateto en el radio y el otro en la mitad de la altura.

altura = 2 * sqrt(maxMod^2 - radio^2);

% %%%%%%%%%%%%%%%%%%%%%%%
%
% Imprimo los resultados de radio y altura
%
% %%%

fprintf('La altura aproximada del cilindro es %f\n', altura);
fprintf('El radio aproximado del cilindro es %f\n', radio);

% %%%%%%%%%%%%%%%%%%%%%%%
%
% Inicio el proceso de determinar la inclinación respecto de Z
%
% %%%

% Para tratar de determinar la inclinación tengo que usar los puntos dados
% del cilindro, ya que no tengo una descripción más precisa del mismo.
%
% Una aproximación trivial podría ser utilizar los puntos más alejados,
% aquellos que tienen máximo y mínimo valor de coordenada Z, y con ellos
% determinar el ángulo de la recta que los une. Como esa recta forma un
% ángulo conocido respecto del vector normal a las tapas del cilindro, eso
% define también el ángulo que forma el cilindro respecto de Z.
%
% El problema de esa aproximación es que debido a la "resolución" con que tengo
% tengo definido el cilindro (cada uno de los puntos) tomar una medida de 
% ángulo a partir de solamente dos puntos puede tener un par de grados de
% error.
%
% En lugar de eso voy a tratar de separar los dos extremos del cilindro, de
% forma de separarlo en dos conjuntos con simetría axial.
% 
% Calculo entonces el centro de masa de cada conjunto, puntos que por
% simetría tienen que estar ubicados sobre la recta que es el eje de
% simetría del cilindro. Al hacer este cálculo sobre una cantidad grande
% de puntos, el error por resolución permanece más acotado.
%
% Con esos dos puntos puedo determinar un vector normal a las tapas, que a
% su vez utilizo para determinar el ángulo deseado.
%

% Para separar las tapas, corto el cilindro con una esfera...

% Pongo el radio de corte de forma tal de dejar los puntos de la tapa que
% están de la mitad del radio hacia afuera.

radioCorte = sqrt((altura/2)^2 + (radio/2)^2);

indexExternos = m > radioCorte;

extxr = xr(indexExternos);
extyr = yr(indexExternos);
extzr = zr(indexExternos);

% Grafico el cilindro cortado para ver que tenga suficiente cantidad de
% puntos todavía.

figure
plot3(extxr, extyr, extzr, '.');
axis equal
grid on
xlabel('Xrel')
ylabel('Yrel')
zlabel('Zrel')
title('Cilindro cortado con una esfera, para dejar los extremos');

% Separo los puntos de cada extremo. Lo sucio de este paso es que no es
% generalizable para cualquier posición del cilindro, porque aprovecho que
% los puntos de uno y otro extremo, con este radio de corte, quedaron con
% diferente signo de Z, lo cual no es general ni por asomo.

indexTapa1 = extzr > 0;
tapa1xr = extxr(indexTapa1);
tapa1yr = extyr(indexTapa1);
tapa1zr = extzr(indexTapa1);
 
indexTapa2 = extzr < 0;
tapa2xr = extxr(indexTapa2);
tapa2yr = extyr(indexTapa2);
tapa2zr = extzr(indexTapa2);

% Grafico las tapas separadas

figure 
subplot(1, 2, 1);
% Tapa 1
plot3(tapa1xr, tapa1yr, tapa1zr, '.');
axis equal
grid on
xlabel('Xrel')
ylabel('Yrel')
zlabel('Zrel')
title('Tapa Superior');

subplot(1, 2, 2); 
% Tapa 1
plot3(tapa2xr, tapa2yr, tapa2zr, '.');
axis equal
grid on
xlabel('Xrel')
ylabel('Yrel')
zlabel('Zrel')
title ('Tapa Inferior');

% Ahora con las tapas separadas y bastante simétricas, calculo el centro de
% masa de cada una de ellas

tapa1xc = mean(tapa1xr);
tapa1yc = mean(tapa1yr);
tapa1zc = mean(tapa1zr);

tapa2xc = mean(tapa2xr);
tapa2yc = mean(tapa2yr);
tapa2zc = mean(tapa2zr);

% y con ese dato determino el vector colineal con el eje del cilindro...
vectorTapas = [ (tapa1xc - tapa2xc) (tapa1yc - tapa2yc) (tapa1zc - tapa2zc)];

% lo normalizo, para tener un vector normal a las tapas...
vectorTapasNorm = vectorTapas / norm(vectorTapas);

% Grafico el vector encontrado en relación al cilindro, para verificar
% que todo siga teniendo sentido.

figure
plot3(extxr, extyr, extzr, '.', [tapa1xc tapa2xc], [tapa1yc tapa2yc], [tapa1zc tapa2zc], '*-');
axis equal
grid on
xlabel('Xrel')
ylabel('Yrel')
zlabel('Zrel')
title('Vector normal a las tapas hallado');

% %%%%%%%%%%%%%%%%%%%%%%%
%
% Finalmente calcula la inclinación respecto del eje Z
%
% %%%

% Ahora queda la parte fácil, calcula la inclinación respecto de la
% vertical (no es el ángulo pedido, pero me sirve para comprobar los pasos
% más tarde).
%
% Para medir los angulos aprovecho que el producto escalar entre vectores
% unitarios es el coseno del angulo que forman.

% Calculo la inclinación respecto de la vertical, o lo que es lo mismo,
% respecto del vector unitario normal al plano XY, [ 0 0 1 ];
%

theta = acos(sum(vectorTapasNorm .* [0 0 1])); 

fprintf('La inclinación aproximada respecto del eje Z es %f rad (%f deg)\n', theta, theta * 180/pi);

% %%%%%%%%%%%%%%%%%%%%%%%
%
% Lo siguiente no se pide, así que no lo imprimo para no generar confusión,
% pero es el ángulo de rotación alrededor de Z
% 
% %%%

% Para determinar la rotación alrededor de Z proyecto el vectorTapas sobre
% el plano XY, lo normalizo, y calculo su ángulo respecto del vector
% unitario que está a lo largo de eje X

proyeccionXYVectorTapas = [ (tapa1xc - tapa2xc) (tapa1yc - tapa2yc) 0]; 
proyeccionXYVectorTapasNorm = proyeccionXYVectorTapas / norm(proyeccionXYVectorTapas);

phi = acos(sum(proyeccionXYVectorTapasNorm .* [1 0 0])); 

% fprintf('La rotación ALREDEDOR del eje Z es %f rad (%f deg)\n', phi, phi * 180/pi);

fprintf('\n');

